from neo4j import GraphDatabase
import pickle
import os

from dotenv import load_dotenv
load_dotenv()

host = os.getenv('NEO4J_HOST')
user = os.getenv('NEO4J_USER')
password = os.getenv('NEO4J_PASSWORD')

driver = GraphDatabase.driver(host, auth=(user, password))

for card in pickle.load(open('eldraine.pickle', 'r')):

    with driver.session() as session:

        def do_tx(tx, card):

            print card

            # Create the card
            parent_result = tx.run("CREATE (a:Card) SET "
                            "a.id = $id, "
                            "a.name = $name "
                            "RETURN id(a)",
                            id=card['id'],
                            name=card['name'],
            ).value()[0]

            evergreen_mechanics = [x.lower() for x in [
                'Deathtouch',
                'Defender',
                'Double',
                'Enchant',
                'First',
                'Flash',
                'Flying',
                'Haste',
                'Hexproof',
                'Indestructible',
                'Lifelink',
                'Menace',
                'Protection',
                'Reach',
                'Trample',
                'Vigilance',
            ]]

            entries = card['oracle_text'].split('\n') if 'oracle_text' in card else None
            if entries == None:
                return

            # Look for evergreen mechanics and split them
            split_entries = [x.strip() for x in entries[0].split(',')]
            if len(split_entries) > 1:
                if False in [True if x.lower().strip() in evergreen_mechanics else False for x in split_entries]:
                    print '[X] Skipping non-evergreen: %s' % entries[0]
                else:
                    print '[*] Adding evergreen for: %s' % entries[0]
                    print '[*] Adding: %s' % split_entries
                    [entries.append(x.capitalize()) for x in split_entries]
                    del entries[0]

            # Create the body text
            for entry in entries:
                result = tx.run(
                    "CREATE (a:BodyText) SET "
                    "a.text = $text "
                    "RETURN id(a)",
                    id=card['id'],
                    text=entry.replace(card['name'], 'CARDNAME')
                ).value()[0]

                result = tx.run(
                    "MATCH (a:Card), (b:BodyText) "
                    "WHERE "
                    "id(a) = $p_id "
                    "AND "
                    "id(b) = $c_id "
                    "CREATE "
                    "(a)-[r:HAS_TEXT]->(b) "
                    "RETURN a",
                    p_id = parent_result,
                    c_id = result
                )


        greeting = session.write_transaction(do_tx, card)

with driver.session() as session:
    def do_merge(tx):
        # Merge duplicate nodes now
        result = tx.run(
            '''MATCH(a:Card)-[r]->(Z:BodyText), (b:Card) - [r2]->(Z2:BodyText)
                WHERE Z.text = Z2.text
                CALL apoc.refactor.mergeNodes([Z, Z2])
                YIELD node
                RETURN node'''
        )

    session.write_transaction(do_merge)

driver.close()



